package com.urlshortener.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.urlshortener.dao.AccountRepository;
import com.urlshortener.data.request.AccountRequest;
import com.urlshortener.data.response.AccountResponse;
import com.urlshortener.entity.Account;
import com.urlshortener.util.RandomStringGeneratorUtil;

/**
 * @author Monil Panchal
 *
 */
@Service
public class AccountService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);

	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private AccountResponse accountResponse;
	@Autowired
	private Account account;

	@Autowired
	BeanFactory beanFactory;

	/**
	 * Service method to register new account.
	 * 
	 * @param accountRequest
	 * @return
	 */
	public AccountResponse registerAccount(AccountRequest accountRequest) {

		String accountId = accountRequest.getAccountId();

		try {

			if (accountId == null) {
				String message = "accountId is mandatory ";
				LOGGER.info(message);

				accountResponse = beanFactory.getBean(AccountResponse.class);

				accountResponse.setDescription(message);
				accountResponse.setSuccess(false);
				accountResponse.setPassword(null);

				throw new Exception(message);
			}

			if (getAccount(accountId) != null) {
				String message = "account with id: " + accountId + " already exists";
				LOGGER.info(message);
				accountResponse = beanFactory.getBean(AccountResponse.class);
				accountResponse.setDescription(message);
				accountResponse.setSuccess(false);
				accountResponse.setPassword(null);

				throw new Exception(message);
			} else {
				LOGGER.info("creating new account");

				account = beanFactory.getBean(Account.class);
				String tempPassword = RandomStringGeneratorUtil.generateTemporaryPassword();
				account.setAccountId(accountId);
				account.setPwd(tempPassword);

				accountRepository.save(account);

				LOGGER.info("account got created: " + account);
				accountResponse = beanFactory.getBean(AccountResponse.class);
				accountResponse.setDescription("Your account is opened. Account ID is: " + accountId);
				accountResponse.setSuccess(true);
				accountResponse.setPassword(tempPassword);

			}
		} catch (Exception e) {
			LOGGER.error(e.toString());
		}

		return accountResponse;
	}

	public Account getAccount(String accountId) {
		LOGGER.info("Fetching details for the accountId: " + accountId);
		account = accountRepository.findByAccountId(accountId);

		return account;
	}

}
