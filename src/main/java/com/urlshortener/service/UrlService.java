package com.urlshortener.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.urlshortener.dao.UrlRepository;
import com.urlshortener.data.request.RegisterUrlRequest;
import com.urlshortener.data.response.ShortUrlResponse;
import com.urlshortener.entity.Account;
import com.urlshortener.entity.Url;
import com.urlshortener.util.RandomStringGeneratorUtil;
import com.urlshortener.util.UrlValidatorUtil;

/**
 * @author Monil Panchal
 *
 */
@Service
public class UrlService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UrlService.class);

	@Value("${server.port}")
	private String serverPort;

	@Value("${server.context-path}")
	private String serverContextPath;

	private static final String SERVER = "http://localhost";

	private static final String SHORT_URL_NAME = "short.url/";

	@Autowired
	private Url url;

	@Autowired
	private ShortUrlResponse shortUrlResponse;

	@Autowired
	private UrlRepository urlRepository;

	@Autowired
	private AccountService accountService;

	@Autowired
	BeanFactory beanFactory;

	/**
	 * Service method to register original url and generate short url
	 * 
	 * @param request
	 * @param loggedInUser
	 * @return
	 */
	public ShortUrlResponse registerUrl(RegisterUrlRequest request, String loggedInUser) {

		String requestUrl = request.getUrl();
		Integer redirectType = request.getRedirectType();

		try {
			if (requestUrl == null) {
				shortUrlResponse = beanFactory.getBean(ShortUrlResponse.class);
				shortUrlResponse.setSuccess(false);
				shortUrlResponse.setDescription("url is mandatory");

				throw new Exception("url in the request is missing/empty. url is mandatory");
			}

			else {
				if (!UrlValidatorUtil.validateUrl(requestUrl)) {
					shortUrlResponse = beanFactory.getBean(ShortUrlResponse.class);
					shortUrlResponse.setSuccess(false);
					shortUrlResponse.setDescription("malformed url");

					throw new Exception("malformed url");
				}

				else {
					url = urlRepository.findByOriginalUrlAndAccountAccountId(requestUrl, loggedInUser);
					if (url != null) {
						shortUrlResponse = beanFactory.getBean(ShortUrlResponse.class);
						shortUrlResponse.setSuccess(false);
						shortUrlResponse.setDescription("This url is already registered.");
						String shortUrl = getShortUrl(url.getEncoding());
						shortUrlResponse.setShortUrl(shortUrl);
						throw new Exception("This url is already registered.");
					}

					LOGGER.info("Url is valid");
					LOGGER.info("Generating short url");
					// String encoding = ShortUrlGenerator.encodeUrl();
					String encoding = RandomStringGeneratorUtil.generateUrlString();
					String encodingUrl = "/" + SHORT_URL_NAME + encoding;
					String shortUrl = getShortUrl(encoding);
					url = beanFactory.getBean(Url.class);

					url.setOriginalUrl(requestUrl);
					url.setShortUrl(encodingUrl);
					url.setHits(0);
					url.setEncoding(encoding);
					url.setAccount(accountService.getAccount(loggedInUser));

					if (redirectType == null || redirectType != 301)
						redirectType = 302;

					url.setRedirectType(redirectType);

					urlRepository.save(url);

					shortUrlResponse = beanFactory.getBean(ShortUrlResponse.class);
					shortUrlResponse.setShortUrl(shortUrl);

					LOGGER.info("URL has been successfully resigtered");
					LOGGER.info("Short url: " + shortUrlResponse.getShortUrl());
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.toString());
		}

		return shortUrlResponse;

	}

	/**
	 * @param encoding
	 * @return
	 */
	public Url accessShortUrl(String encoding) {
		LOGGER.info("fetching details of the registered URL");
		LOGGER.info("encoding: " + encoding);
		try {
			if (encoding != null) {
				url = urlRepository.findByEncoding(encoding);

				if (url != null) {

					LOGGER.info("Registered URL found: " + url);
					int urlHits = url.getHits();

					++urlHits;
					url.setHits(urlHits);

					urlRepository.save(url);
					String shortUrl = getShortUrl(encoding);
					url.setShortUrl(shortUrl);

				} else {
					LOGGER.info("URL does not exists");
					url = null;
					throw new Exception("URL does not exists");
				}

			}
		} catch (Exception e) {
			LOGGER.error(e.toString());
		}
		return url;
	}

	/**
	 * Service method to get list of registered URLs
	 * 
	 * @param account
	 * @return
	 */
	public List<Url> getRegisteredUrl(Account account) {
		List<Url> registeredUrls = new ArrayList<>();
		registeredUrls = urlRepository.findByAccountAccountId(account.getAccountId());
		return registeredUrls;
	}

	private String getShortUrl(String encoding) {
		String encodingUrl = "/" + SHORT_URL_NAME + encoding;
		String shortUrl = SERVER + ":" + serverPort + serverContextPath + encodingUrl;
		return shortUrl;

	}

}
