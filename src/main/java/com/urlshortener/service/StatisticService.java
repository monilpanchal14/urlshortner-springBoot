package com.urlshortener.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.urlshortener.entity.Account;
import com.urlshortener.entity.Url;

@Service
public class StatisticService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UrlService.class);

	@Autowired
	private AccountService accountService;

	@Autowired
	private UrlService urlService;

	/**
	 * Service method for getting list of registered urls with hit count
	 * 
	 * @param accountId
	 * @return
	 */
	public Map<String, Integer> geturlStats(String accountId) {

		Map<String, Integer> urlStat = null;
		List<Url> registeredUrls = null;
		try {

			Account account = accountService.getAccount(accountId);
			if (account != null) {
				urlStat = new HashMap<>();
				registeredUrls = new ArrayList<>();
				registeredUrls = urlService.getRegisteredUrl(account);

				for (Url url : registeredUrls) {
					urlStat.put(url.getOriginalUrl(), url.getHits());
				}
				LOGGER.info("list size of registered urls" + urlStat.size());
				LOGGER.info("list of registered urls with hit counts: \n" + urlStat);
			}

			else {
				throw new Exception("accountId not registed");

			}

		} catch (Exception e) {
			LOGGER.error(e.toString());
		}
		return urlStat;
	}
}