package com.urlshortener.util;

import java.security.SecureRandom;

public class RandomStringGeneratorUtil {

	private static final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private static SecureRandom rnd = new SecureRandom();

	public static String generateTemporaryPassword() {
		String tempPassword = randomString(8);
		return tempPassword;
	}

	public static String generateUrlString() {
		String tempPassword = randomString(6);
		return tempPassword;
	}

	private static String randomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(ALPHABET.charAt(rnd.nextInt(ALPHABET.length())));
		return sb.toString();
	}
}