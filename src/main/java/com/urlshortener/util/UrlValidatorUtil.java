package com.urlshortener.util;

import org.apache.commons.validator.routines.UrlValidator;

/**
 * @author Monil Panchal
 *
 */
public class UrlValidatorUtil {

	public static boolean validateUrl(String url) {

		String[] schemes = { "http", "https" };
		UrlValidator urlValidator = new UrlValidator(schemes);
		if (urlValidator.isValid(url)) {
			return true;
		} else {
			return false;
		}
	}
}