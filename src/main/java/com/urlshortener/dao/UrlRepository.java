package com.urlshortener.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.urlshortener.entity.Url;

public interface UrlRepository extends CrudRepository<Url, Long> {

	public Url findByOriginalUrl(String originalUrl);

	public Url findByEncoding(String encoding);

	public List<Url> findByAccountAccountId(String accountId);

	public Url findByOriginalUrlAndAccountAccountId(String originalUrl, String accountId);

}
