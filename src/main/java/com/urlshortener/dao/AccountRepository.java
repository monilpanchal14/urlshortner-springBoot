package com.urlshortener.dao;

import org.springframework.data.repository.CrudRepository;

import com.urlshortener.entity.Account;

public interface AccountRepository extends CrudRepository<Account, Long> {
	
	public Account findByAccountId(String accountId);

}
