package com.urlshortener.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.urlshortener.service.StatisticService;

@RestController
public class StatisticController {

	@Autowired
	private StatisticService statisticController;

	/**
	 * API controller method for getting statistics of registered url. The
	 * result is list of original urls and hit accounts registered with
	 * accountId.
	 * 
	 * @param accountId
	 * @return
	 */
	@RequestMapping(value = "/statistic/{accountId}", method = RequestMethod.GET)
	public ResponseEntity<?> geturlStats(@PathVariable("accountId") String accountId) {
		Map<String, Integer> urlStats = new HashMap<>();
		urlStats = statisticController.geturlStats(accountId);
		if (urlStats == null || urlStats.isEmpty()) {
			return new ResponseEntity<String>("accountId: " + accountId + " not found", HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Map<String, Integer>>(urlStats, HttpStatus.OK);
	}

}
