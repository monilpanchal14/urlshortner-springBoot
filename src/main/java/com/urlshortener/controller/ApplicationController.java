package com.urlshortener.controller;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationController {

	@RequestMapping(value = "/help", method = RequestMethod.GET)
	public ResponseEntity<Object> redirect(HttpServletResponse response) throws IOException, URISyntaxException {
		URI redirection = new URI("html/help.html");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(redirection);

		return new ResponseEntity<>(httpHeaders, HttpStatus.PERMANENT_REDIRECT);

	}

}
