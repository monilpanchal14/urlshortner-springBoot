package com.urlshortener.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.urlshortener.data.request.AccountRequest;
import com.urlshortener.data.response.AccountResponse;
import com.urlshortener.service.AccountService;

/**
 * @author Monil Panchal
 *
 */
@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private AccountResponse accountResponse;

	/**
	 * API controller to register new account
	 * 
	 * METHOD: POST
	 * 
	 * @param accountRequest
	 * @return
	 */
	@RequestMapping(value = "/account", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AccountResponse> registerAccount(@RequestBody AccountRequest accountRequest) {

		HttpStatus httpStatus = null;
		accountResponse = accountService.registerAccount(accountRequest);

		if (accountResponse == null) {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			return new ResponseEntity<>(httpStatus);
		}
		if (accountResponse.isSuccess())
			httpStatus = HttpStatus.CREATED;

		else
			httpStatus = HttpStatus.CONFLICT;

		return new ResponseEntity<AccountResponse>(accountResponse, httpStatus);
	}
}