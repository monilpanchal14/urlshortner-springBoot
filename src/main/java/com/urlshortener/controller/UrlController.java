package com.urlshortener.controller;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.urlshortener.data.request.RegisterUrlRequest;
import com.urlshortener.data.response.ShortUrlResponse;
import com.urlshortener.entity.Url;
import com.urlshortener.service.UrlService;

@RestController
public class UrlController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UrlController.class);

	@Autowired
	private UrlService urlService;

	@Autowired
	private ShortUrlResponse shortUrlResponse;

	/**
	 * API controller to registero new URL. Post successful registration, it
	 * generates short url.
	 * 
	 * METHOD: POST
	 * 
	 * @param registerUrlRequest
	 * @return
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ShortUrlResponse> registerUrl(@RequestBody RegisterUrlRequest registerUrlRequest) {
		String username = null;
		try{
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		 username = ((UserDetails) principal).getUsername();
		}catch(Exception e){
			username = "dummy_user";
		}
		LOGGER.info("Logged in user : " + username);

		HttpStatus httpStatus = HttpStatus.OK;
		shortUrlResponse = urlService.registerUrl(registerUrlRequest, username);

		if (shortUrlResponse == null) {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			return new ResponseEntity<>(httpStatus);
		}

		else {
			if (shortUrlResponse.getShortUrl() != null
					&& (shortUrlResponse.isSuccess() != null && !shortUrlResponse.isSuccess())) {
				httpStatus = HttpStatus.CONFLICT;
			}

			else if (shortUrlResponse.getShortUrl() != null && shortUrlResponse.isSuccess() == null) {
				httpStatus = HttpStatus.CREATED;

			} else if (shortUrlResponse.isSuccess() != null && !shortUrlResponse.isSuccess()) {
				httpStatus = HttpStatus.CONFLICT;
			}
		}

		return new ResponseEntity<ShortUrlResponse>(shortUrlResponse, httpStatus);

	}

	/**
	 * API controllerMethod for redirecting the short url to the registed
	 * original url
	 * 
	 * METHOD: GET
	 * 
	 * @param encoding
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@RequestMapping(value = "/short.url/{encoding}", method = RequestMethod.GET)
	public ResponseEntity<Object> redirect(@PathVariable("encoding") String encoding, HttpServletResponse response)
			throws IOException, URISyntaxException {
		URI redirection = null;
		String originalUrl = null;
		HttpHeaders httpHeaders;

		Url url = urlService.accessShortUrl(encoding);

		if (url != null) {
			originalUrl = url.getOriginalUrl();
			redirection = new URI(originalUrl);
			httpHeaders = new HttpHeaders();
			httpHeaders.setLocation(redirection);

			return new ResponseEntity<>(httpHeaders, HttpStatus.valueOf(url.getRedirectType()));
		}
		return new ResponseEntity<>("Invalid url", HttpStatus.BAD_REQUEST);

	}

}
