package com.urlshortener.data.response;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope(value = "prototype")
@JsonInclude(Include.NON_NULL)
public class ShortUrlResponse {

	@JsonProperty("shortUrl")
	private String shortUrl;

	@JsonProperty("success")
	private Boolean success;
	@JsonProperty("description")
	private String description;

	public String getShortUrl() {
		return shortUrl;
	}

	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}

	public Boolean isSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "ShortUrlResponse [shortUrl=" + shortUrl + ", success=" + success + ", description=" + description + "]";
	}

}
