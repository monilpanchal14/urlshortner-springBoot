package com.urlshortener.test.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.urlshortener.controller.UrlController;
import com.urlshortener.data.request.RegisterUrlRequest;
import com.urlshortener.data.response.ShortUrlResponse;
import com.urlshortener.service.UrlService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UrlController.class, secure = false)
public class UrlControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UrlService urlService;

	@MockBean
	private ShortUrlResponse shortUrlResponse;

	@Test
	public void registerUrl_successful() throws Exception {

		shortUrlResponse = new ShortUrlResponse();
		shortUrlResponse.setShortUrl("http://localhost:9099/urlshortener/short.url/uM1IeO");

		// studentService.addCourse to respond back with mockCourse
		Mockito.when(urlService.registerUrl(Mockito.any(RegisterUrlRequest.class), Mockito.anyString()))
				.thenReturn(shortUrlResponse);

		// Send course as body to /students/Student1/courses
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/register")
				.accept(MediaType.APPLICATION_JSON_VALUE).content(
						"{\"url\":\"https://www.google.co.in/search?q=google&oq=google&aqs=chrome..69i57j69i60l3j0j69i60.4289j0j7&sourceid=chrome&ie=UTF-8#q=google&tbm=nws\"}")

				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		String content = response.getContentAsString();
		String expected = "{\"shortUrl\": \"http://localhost:9099/urlshortener/short.url/uM1IeO\"}";

		assertEquals(HttpStatus.CREATED.value(), response.getStatus());

		JSONAssert.assertEquals(expected, content, false);

	}

	@Test
	public void registerUrl_unsuccessful_duplicate() throws Exception {

		shortUrlResponse = new ShortUrlResponse();
		shortUrlResponse.setShortUrl("http://localhost:9099/urlshortener/short.url/uM1IeO");
		shortUrlResponse.setSuccess(false);
		shortUrlResponse.setDescription("This url is already registered.");

		// studentService.addCourse to respond back with mockCourse
		Mockito.when(urlService.registerUrl(Mockito.any(RegisterUrlRequest.class), Mockito.anyString()))
				.thenReturn(shortUrlResponse);

		// Send course as body to /students/Student1/courses
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/register")
				.accept(MediaType.APPLICATION_JSON_VALUE).content(
						"{\"url\":\"https://www.google.co.in/search?q=google&oq=google&aqs=chrome..69i57j69i60l3j0j69i60.4289j0j7&sourceid=chrome&ie=UTF-8#q=google&tbm=nws\"}")

				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		String content = response.getContentAsString();
		String expected = "{\"shortUrl\": \"http://localhost:9099/urlshortener/short.url/uM1IeO\",\"success\": false,\"description\": \"This url is already registered.\"}";

		assertEquals(HttpStatus.CONFLICT.value(), response.getStatus());

		JSONAssert.assertEquals(expected, content, false);

	}

	@Test
	public void registerUrl_invalid() throws Exception {

		shortUrlResponse = new ShortUrlResponse();
		shortUrlResponse.setSuccess(false);
		shortUrlResponse.setDescription("url is mandatory");

		// studentService.addCourse to respond back with mockCourse
		Mockito.when(urlService.registerUrl(Mockito.any(RegisterUrlRequest.class), Mockito.anyString()))
				.thenReturn(shortUrlResponse);

		// Send course as body to /students/Student1/courses
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/register")
				.accept(MediaType.APPLICATION_JSON_VALUE).content(
						"{\"new-url\":\"https://www.google.co.in/search?q=google&oq=google&aqs=chrome..69i57j69i60l3j0j69i60.4289j0j7&sourceid=chrome&ie=UTF-8#q=google&tbm=nws\"}")

				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		String content = response.getContentAsString();
		String expected = "{\"success\": false,\"description\": \"url is mandatory\"}";

		assertEquals(HttpStatus.CONFLICT.value(), response.getStatus());

		JSONAssert.assertEquals(expected, content, false);

	}

}
