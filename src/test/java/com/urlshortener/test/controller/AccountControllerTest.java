package com.urlshortener.test.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.urlshortener.controller.AccountController;
import com.urlshortener.data.request.AccountRequest;
import com.urlshortener.data.response.AccountResponse;
import com.urlshortener.service.AccountService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = AccountController.class, secure = false)
public class AccountControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AccountService accountService;

	@MockBean
	private AccountResponse accountResponse;

	@Test
	public void registerAccount_successful() throws Exception {

		accountResponse = new AccountResponse();
		accountResponse.setDescription("Your account is opened. Account ID is: user_1");
		accountResponse.setPassword("abcdef12");
		accountResponse.setSuccess(true);

		// studentService.addCourse to respond back with mockCourse
		Mockito.when(accountService.registerAccount(Mockito.any(AccountRequest.class))).thenReturn(accountResponse);

		// Send course as body to /students/Student1/courses
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/account").accept(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"accountId\":\"user_1\"}")

				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		String content = response.getContentAsString();
		String expected = "{\"success\": true,\"description\": \"Your account is opened. Account ID is: user_1\",\"password\": \"abcdef12\"}";

		assertEquals(HttpStatus.CREATED.value(), response.getStatus());

		JSONAssert.assertEquals(expected, content, false);

	}

	@Test
	public void registerAccount_unsuccessful_duplicate() throws Exception {

		accountResponse = new AccountResponse();
		accountResponse.setDescription("account with id: user_1 already exists");
		accountResponse.setSuccess(false);

		// studentService.addCourse to respond back with mockCourse
		Mockito.when(accountService.registerAccount(Mockito.any(AccountRequest.class))).thenReturn(accountResponse);

		// Send course as body to /students/Student1/courses
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/account").accept(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"accountId\":\"user_1\"}")

				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		String content = response.getContentAsString();
		String expected = "{\"success\": false,\"description\": \"account with id: user_1 already exists\"}";

		assertEquals(HttpStatus.CONFLICT.value(), response.getStatus());

		JSONAssert.assertEquals(expected, content, false);

	}

	@Test
	public void registerAccount_invalid() throws Exception {

		accountResponse = new AccountResponse();
		accountResponse.setDescription("accountId is mandatory");
		accountResponse.setSuccess(false);

		// studentService.addCourse to respond back with mockCourse
		Mockito.when(accountService.registerAccount(Mockito.any(AccountRequest.class))).thenReturn(accountResponse);

		// Send course as body to /students/Student1/courses
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/account").accept(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"account\":\"user_1\"}")

				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		String content = response.getContentAsString();
		String expected = "{\"success\": false,\"description\": \"accountId is mandatory\"}";

		assertEquals(HttpStatus.CONFLICT.value(), response.getStatus());

		JSONAssert.assertEquals(expected, content, false);

	}

}
